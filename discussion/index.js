console.log("Hello world")

/*javascript can also command our browsers to perform different arithmetic operations just like how math works*/

// Arithmetic Operations Section
// creation of variables to use in math operations
// + addition
// - subtraction
// * multiplication
// / division
// % modulo (remainder)
let x = 1397;
let y = 7831;

let sum = x+y;
console.log(sum);

let difference = x-y;
console.log(difference);

let product = x*y;
console.log(product);

let quotient = x/y;
console.log(quotient);

let remainder = y%x;
console.log(remainder);

// Assignment Operator
// = assignment operator and is used to assign a value to a variable; the value on the right side of the operator is assigned to the left variable
let assignmentNumber = 8;

// addition assignment operator. this is the long way of putting it:
// assignmentNumber = assignmentNumber + 2;
// below is the shorthand (+=). walang space dapat between + and = dito kasi mag-error siya
assignmentNumber += 2;
console.log(assignmentNumber);

// subtraction assignment operator
assignmentNumber -= 2;
console.log(assignmentNumber);
// 8 ang sagot not 6 kasi kinuha na niya yung value nung bagong assignment number nung preceding operation which is 10 kaya 8 ang sagot dito. taas pababa kasi ang direction ng pagbasa ni JS

// multiplication assignment operator
assignmentNumber *= 2;
console.log(assignmentNumber);

// division assignment operator
assignmentNumber /= 2;
console.log(assignmentNumber);

// Multiple operators and parenthesis
// when multiple operators are present in a single statement, it follows the PEMDAS (Parenthesis, Exponent, Multiplication, Division, Addition, Subtraction)
let mdas = 1+2-3*4/5;
console.log(mdas);

let pemdas = 1+(2-3)*4/5;
console.log(pemdas);

pemdas = (1+(2-3))*(4/5);
console.log(pemdas);

// Increment and Decrement section
// assigning a value to a variable to be used in incremenet and decrement
let z = 1;

// increment (++) is adding 1 to the value of the variable whether before or after the assigning of value
	// before assigning of value - tawag dun pre-increment (adding 1 to the value before it is assigned to the variable)
	// post-increment 0 adding 1 to the b=vaue after it is assigned to the variable
let increment = ++z;
// the value of z is added by a value of 1 before returning the value and storing it inside the variable
console.log("Result of pre-increment:" + increment);

// the value of z was also increased by 1 even though we didn't explicitly specify any value reassignment 
console.log("Result of pre-increment:" + z);
// so, if gagawing 5 si z, magiging 6 ang sagot dito sa sample sa taas

increment=z++;
// the value of z is at 2 before it was incremented
console.log("Result of post-increment:" + increment);
// the value of z was increased again reassigning the value to 3
console.log("Result of post-increment:" + z);

// decrement (--) is subtracting 1 to the value whether before or after assigning to the variable
	// pre-decrement - subtracting 1 to the value before it is assigned to the variable
	// post-decrement is subtracting 1 to the variable after it is assigned to the variable
let decrement = --z;
// the value of z is at 3 before it was decremented
console.log("Result of pre-decrement:" + decrement);
// the value of z was reassigned to 2
console.log("Result of pre-decrement:" + z);

decrement = z--;
// the value of z was 2 before it was decremented 
console.log("Result of post-decrement:" + decrement);
// the value of z was decreased and reassigned to 1
console.log("Result of post-decrement:" + z);
// sa latest value ng variable siya nagdedepend

// Type Coercion
// is the automatic or implicit converson of values from one data type to another. This happens when operations performed on different data types that would normally nt be possible and yield irregular result
// values are automatically assigned/converted from 1 data type to another in order to resolve operations

let numbA ='10';
let numbB = 12;
let coercion = numbA + numbB;
console.log(coercion);

// pinagsama lang niya (1012) kasi string data type si numbA (10) kahit technically number siya kaya hindi niya ma-add kay 12. String data type cannot be included in any mathematical operation

// try to have type coercion for the following:
/*number data + number data = integer or number
boolean + number = integer or number
boolean + string = string*/

// num + num
let numbC = 16;
let numbD = 14;

let nonCoercion = numbC + numbD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// boolean + num
// boolean is just like binary in javascript
	// true = 1 and false = 0
let numbE = true + 1;
console.log(numbE);
console.log(typeof numbE);

// boolean + string
let varA="String plus " + true;
console.log(varA);
console.log(typeof varA);

// Comparison Operators

// equality operators
	// it checks whether the operands are equal or have the same content; pareho ba sila?
console.log(1==1);
console.log(1==2);
console.log(1=="1");
console.log(1==true);
console.log('juan'=='juan');
let juan = 'juan';
console.log('juan'==juan);

// inequality operator
// it checks whether the operands are inequal or does not have the same content; magkaiba ba sila?
console.log(1!=1);
console.log(1!=2);
console.log(1!='1');
console.log(0!=false);
console.log("juan"!='juan');
console.log('juan'!=juan);

// strict equality/inequality Operators
// this also check the contents of the operands but aside from that, it also checks/compares the data types of the 2 operands
// JS is a loosely typed language, meaning that the values of difference data type can be stored inside a variable; tinitingnan niya anong content nung ginawa nating variable instead of the variable itself kaya true pa rin yun 'juan'===juan
// strict operators are better to be used in most cases to ensure that the data types provided are correct

// strict equality operator
console.log(1===1);
console.log(1===2);
console.log(1==="1");
console.log(1===true);
console.log('juan'==='juan');
console.log('juan'===juan);

// strict inequality operator
console.log(1!==1);
console.log(1!==2);
console.log(1!=='1');
console.log(0!==false);
console.log("juan"!=='juan');
console.log('juan'!==juan);

// Relational operators
// some comparison operators check whether one value is greater or less than to the other value
// just like equality/inqeuality operators, they return boolean based on the assessment of the two values

let a=50;
let b=65;

let greaterThan=a>b;
// GT/greater than >
let lessThan=a<b;
// LT/less than <
let greaterThanOrEqualTo=a>=b;
// GTE/greater than or equal to >=
let lessThanOrEqualTo=a<=b;
// LTE/less than or equal to <=

console.log(greaterThan);
console.log(lessThan);
console.log(greaterThanOrEqualTo);
console.log(lessThanOrEqualTo);

// thhis returned true as a product of a forced coercion to change the string into a number of date type
let numStr="30";
console.log(a>numStr);


// since a string is not numeric, the string was converted into a number and it resulted into a NaN (Not a Number); kaya false ang naging return niya, kasi di mo naman macompare ang number sa NaN
	// NaN is the result of an unsuccessful conversion of string into number data type of an alphanumeric string
let str="twenty";
console.log(b>=str);

// Logical Operators
// checking whether the values of two or more variables are true/false

let isLegalAge=true;
let isRegistered= false;

// And Operator(&&)
// returns true if all values are true
// 1         2          end result
// true      true       true
// true      false      false
// false     false      false
// false     true       false
let allRequirementsMet = isLegalAge&&isRegistered;
console.log("Result of And Operator: " + allRequirementsMet);

// Or Operator (||)

	/*1 		2 		end result
	true 	true 	true
	true 	false	true
	false 	true 	true
	false 	false	false*/


// Not Operator
// returns the opposite of the value
/*
!true = false
!false = true
*/

let someRequirementsNotMet = !isRegistered;
console.log("Result of Not Operator: " + someRequirementsNotMet);

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of Or Operator: " + someRequirementsMet);






